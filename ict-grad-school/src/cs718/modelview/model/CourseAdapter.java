package cs718.modelview.model;

import javax.swing.table.AbstractTableModel;


/**
 * An adapter which allows a Course model to be rendered using a JTable Swing
 * component. CourseAdapter implements (indirectly) Swing's TableModel
 * interface.
 * 
 */
public class CourseAdapter  extends AbstractTableModel implements CourseListener {

	/* Ordered column header names. */
	private static String[] columnNames = { "Sudent ID", "Surname", "Forename",
			"Exam", "Test", "Assignment", "Overall" };

	private Course _adaptee;

	/**
	 * Creates a CourseAdapter object.
	 */
	public CourseAdapter(Course course) {
		_adaptee = course;
	}

	/**
	 * Returns the number of columns in a CourseAdapter object.
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/**
	 * Returns the number of rows in a CourseAdapter object. This equates to the
	 * number of student result records stored in the adaptee.
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return _adaptee.size();
	}

	/**
	 * Returns the object held at a specified cell within a CourseAdapter
	 * instance. The returned object is the value of StudentResult object's
	 * instance variable.
	 * 
	 * @param row
	 *            the row at which the StudentResult object resides.
	 * @param col
	 *            the column containing the required attribute of the
	 *            StudentResult object identified by the row argument.
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int col) {
		/*
		 * Get the StudentResult object at the specified row.
		 */
		StudentResult result = (StudentResult) _adaptee.getResultAt(row);
		Object value = null;

		/*
		 * Get the StudentResult attribute at the specified column.
		 */
		switch (col) {
		case 0: // Student ID.
			value = result._studentID;
			// new Integer( result.studentID );
			break;
		case 1: // Surname.
			value = result._studentSurname;
			break;
		case 2: // Forename.
			value = result._studentForename;
			break;
		case 3: // Exam.
			value = result
					.getAssessmentElement(StudentResult.AssessmentElement.Exam);
			break;
		case 4: // Test.
			value = result
					.getAssessmentElement(StudentResult.AssessmentElement.Test);
			break;
		case 5: // Assignment.
			value = result
					.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			break;
		case 6: // Overall.
			value = result
					.getAssessmentElement(StudentResult.AssessmentElement.Overall);
			break;
		}
		return value;
	}

	/**
	 * Returns the column name of a particular column.
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/**
	 * Returns false - no cells are editable.
	 * 
	 * @param row
	 *            the row of the specified cell.
	 * @param col
	 *            the column of the specified cell.
	 */
	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	/**
	 * Returns the Class of object shown in a specified column. In the case of a
	 * CourseAdapter object: 0 (student ID) = Integer, 1 (Surname) = String, 2
	 * (Forename) = String, 3..6 = Percentage.
	 */
	@Override
	public Class<?> getColumnClass(int col) {
		Class<?> columnClass = null;

		if (col == 0) {
			columnClass = Integer.class;
		}
		if (col == 1 || col == 2) {
			columnClass = String.class;
		} else {
			columnClass = Percentage.class;
		}
		return columnClass;
	}
	
	/**
	 * Listener method, invoked when a Course object changes its state.
	 */
	@Override
	public void courseHasChanged(Course course) {
		// Fire a TableModelEvent so that this CourseAdapter's JTable view is
		// updated.
		fireTableDataChanged();
	}

}
