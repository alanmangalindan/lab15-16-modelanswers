package cs718.modelview.policy;

import cs718.modelview.model.StudentResult;
import cs718.modelview.model.Percentage;

/**
 * A course assessment policy. This policy is used to assess students entirely
 * on their assignment mark.
 * 
 */
public class ZeroZeroOneHundred implements AssessmentPolicy {

	public Percentage calculate(StudentResult result) {
		return result
				.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
	}

	public String toString() {
		return "Assignment only";
	}

}