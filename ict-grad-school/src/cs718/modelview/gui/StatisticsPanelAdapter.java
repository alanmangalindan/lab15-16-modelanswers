package cs718.modelview.gui;

import cs718.modelview.model.Course;
import cs718.modelview.model.CourseListener;

/**
 * Adapter class that allows a StatisticsPanel to work with a Course. A
 * StatisticsPanelAdapter instance can be registered as a listener on a
 * Course.
 * 
 */
public class StatisticsPanelAdapter implements CourseListener {

	private StatisticsPanel _adaptee;

	public StatisticsPanelAdapter(StatisticsPanel view) {
		_adaptee = view;
	}

	@Override
	public void courseHasChanged(Course course) {
		_adaptee.repaint();
		
	}

}
