package cs718.modelview.gui;

import cs718.modelview.model.Course;
import cs718.modelview.model.CourseListener;

/**
 * Adapter class to allow a DistributionPanel to receive updates from a
 * Course.
 * 
 */
public class DistributionPanelAdapter implements CourseListener {

	private DistributionPanel _adaptee;

	public DistributionPanelAdapter(DistributionPanel view) {
		_adaptee = view;
	}

	@Override
	public void courseHasChanged(Course course) {
		/*
		 * Request that the DistributionPanel instance repaints itself. This
		 * will case the graph to be redrawn.
		 */
		_adaptee.repaint();
	}

}
